var $window = $(window);
var $videoWrap = $('.video-wrap');
var $video = $('.video');
var videoHeight = $video.outerHeight();

$window.on('scroll',  function() {
  var windowScrollBottom = $(window).scrollTop() + $(window).height();
  var videoTop = videoHeight + $videoWrap.offset().top;
  if (windowScrollBottom < videoTop) {
    $videoWrap.height(videoHeight);
    $video.addClass('stuck');
  } else {
    $videoWrap.height('auto');
    $video.removeClass('stuck');
  }
});
